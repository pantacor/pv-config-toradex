TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := arm

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-musleabihf/bin/arm-linux-musleabihf-

TARGET_GLOBAL_CFLAGS += -march=armv7-a

TARGET_LINUX_IMAGE := zImage

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

LINUX_CONFIG_TARGET := imx_v6_v7_defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

TARGET_UBI_IMAGE_OPTIONS := \
       --mkubifs="-m 1 -e 65408 -c 247 -x favor_lzo -F" \
       --ubinize="-p 0x10000 -m 1 $(TARGET_CONFIG_DIR)/ubinize.config"

# you need to overload this in product.$subtarget.mk - this one is intentionally bogus
# so the build will fail if you haven't configure proper dtb
TARGET_LINUX_DEVICE_TREE := YAYIDONTEXIST-CONFIGUREME.dtb

