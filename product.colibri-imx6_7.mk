
TARGET_LINUX_DEVICE_TREE :=
TARGET_LINUX_DEVICE_TREE_NAMES := \
	imx6ull-colibri-eval-v3.dtb \
	imx6ull-colibri-wifi-eval-v3.dtb \
	imx6dl-colibri-eval-v3.dtb \
	imx7s-colibri-eval-v3.dtb \
	imx7d-colibri-emmc-eval-v3.dtb \
	imx7d-colibri-eval-v3.dtb \
	$(NULL)

