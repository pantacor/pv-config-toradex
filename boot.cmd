env set saveenv_pv_mmc 'env export -s 0x400 ${loadaddr} pv_trying; save mmc ${devnum}:1 ${loadaddr} pv.env 0x400'
env set saveenv_pv_ubi 'env export -s 0x400 ${loadaddr} pv_trying; ubi writevol ${loadaddr} pvenv 0x400'
env set saveenv_pv 'if test "${pv_fstype}" = "ubi"; then run saveenv_pv_ubi; else run saveenv_pv_mmc; fi'
env set baseargs 'root=/dev/ram rootfstype=ramfs init=/init ubi.mtd=ubi'
env set findmmc 'part size mmc ${devnum} 2 config_size; if test "${config_size}" = "800"; then part start mmc ${devnum} 2 config_start; setenv mmcdata 3; mmc read ${loadaddr} ${config_start} ${config_size}; env import ${loadaddr} 0x1000; else setenv mmcdata 2; fi'
env set loadfdt_mmc 'load ${devtype} ${devnum}:${mmcdata} ${fdt_addr_r} "/trails/${boot_rev}/bsp/${fdtfile}"'
env set loadfdt_ubi 'ubifsload ${fdt_addr_r} "/trails/${boot_rev}/bsp/${fdtfile}"'
env set loadfdt 'if test "${pv_fstype}" = "ubi"; then run loadfdt_ubi; else run loadfdt_mmc; fi'
env set loadargs 'setenv bootargs "${mtdparts} ${baseargs} pv_try=${pv_try} pv_rev=${boot_rev} panic=2 ${args} ${setupargs} ${localargs}"'
env set loadenv_mmc 'run findmmc; load mmc ${devnum}:${mmcdata} ${loadaddr} /boot/uboot.txt'
env set loadenv_ubi 'ubifsload ${loadaddr} /boot/uboot.txt'
env set loadpvenv_mmc 'run findmmc; load mmc ${devnum}:1 ${loadaddr} pv.env || mw ${loadaddr} 0 0x400'
env set loadpvenv_ubi 'ubi readvol ${loadaddr} pvenv 0x400'
env set loadpvenv 'if test "${pv_fstype}" = "ubi"; then run loadpvenv_ubi; else run loadpvenv_mmc; fi; env import ${loadaddr} 0x400'
env set loadenv 'if test "${pv_fstype}" = "ubi"; then run loadenv_ubi; else run loadenv_mmc; fi; setenv pv_try; env import ${loadaddr} 0x400; run loadpvenv; if env exists pv_try; then if env exists pv_trying && test ${pv_trying} = ${pv_try}; then setenv pv_trying; run saveenv_pv; setenv boot_rev ${pv_rev}; else setenv pv_trying ${pv_try}; run saveenv_pv; setenv boot_rev ${pv_trying}; fi; else setenv boot_rev ${pv_rev}; fi;'
env set loadstep_mmc 'run loadenv; load mmc ${devnum}:${mmcdata} ${kernel_addr_r} /trails/${boot_rev}/.pv/pv-kernel.img; load mmc ${devnum}:${mmcdata} ${ramdisk_addr_r} /trails/${boot_rev}/.pv/pv-initrd.img; setenv ramdisk_size ${filesize}; setexpr rd_offset ${ramdisk_addr_r} + ${ramdisk_size}; setenv i 0; while load mmc ${devnum}:${mmcdata} ${rd_offset} /trails/${boot_rev}/.pv/pv-initrd.img.${i}; do setexpr i ${i} + 1; setexpr ramdisk_size ${ramdisk_size} + ${filesize}; setexpr rd_offset ${rd_offset} + ${filesize}; done'
env set loadstep_ubi 'ubifsmount ubi0:${pv_ubirootfs}; run loadenv; ubifsload ${kernel_addr_r} /trails/${boot_rev}/.pv/pv-kernel.img; ubifsload ${ramdisk_addr_r} /trails/${boot_rev}/.pv/pv-initrd.img; setenv ramdisk_size ${filesize}; setexpr rd_offset ${ramdisk_addr_r} + ${ramdisk_size}; setenv i 0; while ubifsload ${rd_offset} /trails/${boot_rev}/.pv/pv-initrd.img.${i}; do setexpr i ${i} + 1; setexpr ramdisk_size ${ramdisk_size} + ${filesize}; setexpr rd_offset ${rd_offset} + ${filesize}; done'
env set loadstep 'if test "${pv_fstype}" = "ubi"; then run loadstep_ubi; else run loadstep_mmc; fi'
env set bootcmd_run 'run loadstep; run loadfdt; run loadargs; if test -n "${pv_localrun}"; then run pv_localrun; fi; echo bootz ${kernel_addr_r} ${ramdisk_addr_r}:${ramdisk_size} ${fdt_addr_r}; bootz ${kernel_addr_r} ${ramdisk_addr_r}:${ramdisk_size} ${fdt_addr_r}; echo "Failed to boot step, rebooting"; sleep 6'
env set pv_ubipart "ubi"
env set pv_ubirootfs "pvroot"
env set pv_ubibootfs "pvboot"
if test -z "${pv_noautorun}"; then run bootcmd_run; fi

